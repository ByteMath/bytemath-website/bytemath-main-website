/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: 'class',
  content: [
    "./src/**/*.{html,js}",
    "./public/**/*.html",
],
  theme: {
    extend: {
      colors: {
        'bytemath' : '#7AC5F6',
        'primary': 'gray-400',
				'secondary': '#ef4444',
        'green' : '#01bfa6',
      },
      fontFamily: {
        'noe': ['Noe Display', 'sans-serif'],
        'merriweather': ['Merriweather', 'serif'],
      }
    },
  },
  plugins: [
    require("flowbite/plugin"),
    require('@tailwindcss/typography'),
  ],
}

