import React from 'react'
import PropTypes from 'prop-types'
import { BlogPostTemplate } from '../../templates/blog-post'

const BlogPostPreview = ({ entry, widgetFor, getAsset }) => {
  const tags = entry.getIn(['data', 'tags']);
  const data = entry.getIn(['data']).toJS();

  return (
    <BlogPostTemplate
      reference={entry.getIn(['data', 'reference'])}
      content={widgetFor('body')}
      description={widgetFor('description')}
      tags={tags && tags.toJS()}
      title={entry.getIn(['data', 'title'])}
      image={getAsset(data.image)}
    />
  )
}

BlogPostPreview.propTypes = {
  entry: PropTypes.shape({
    getIn: PropTypes.func,
  }),
  widgetFor: PropTypes.func,
  getAsset: PropTypes.func,
}

export default BlogPostPreview
