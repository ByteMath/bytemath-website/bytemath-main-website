---
templateKey: project-post
title: "Introducing ByMaNiM: Effortless Reveal.js and Manim Presentations"
date: 2024-06-17T06:38:02.694Z
description: |
  `ByMaNiM` is a framework for creating manim-reveal.js slides.
featuredpost: true
featuredimage: https://www.notion.so/image/https:%2F%2Fs3-us-west-2.amazonaws.com%2Fsecure.notion-static.com%2F204a731b-3107-427b-97f2-265e52a9a85d%2Freveal-og-card-1200-630.png?table=block&id=a7d2559e-790b-4ae0-86fc-a54e2ded7434&width=2050&userId=&cache=v2
tags:
  - ByMaNiM
  - python
---
[![PyPI - Version](https://img.shields.io/pypi/v/bymanim?style=plastic&label=ByMaNiM&labelColor=green&color=blue&link=https%3A%2F%2Fbytemath.gitlab.io%2Fpython%2FByMaNiM%2Findex.html)](https://pypi.org/project/bymanim/)

[![GitLab Repository](https://img.shields.io/badge/GitLab-Repository-orange?style=for-the-badge&logo=gitlab)](https://gitlab.com/ByteMath/python/bymanim)


## Description

Say hello to **ByMaNiM**, a powerful framework designed to revolutionize the way you create presentations with Reveal.js and Manim. ByMaNiM combines the elegance of Manim's mathematical animations with the sleek, interactive capabilities of Reveal.js, all wrapped up in a user-friendly package.

Our Python package comes with handy shortcuts and functions specifically for Manim, making it easier than ever to create stunning animations. Additionally, the repository includes a starting framework for integrating Manim with Reveal.js using Vite, ensuring you can get up and running quickly. With ByMaNiM, you can effortlessly craft organized and engaging presentations, whether for mathematics or any other subject, and captivate your audience with visually compelling content.

Get started with ByMaNiM today and experience the simplicity and efficiency of creating professional presentations like never before.
