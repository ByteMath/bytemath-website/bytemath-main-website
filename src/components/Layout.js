import * as React from "react";
import { Helmet } from "react-helmet";
import Footer from "../components/Footer";
import Navbar from "../components/Navbar";
import useSiteMetadata from "./SiteMetadata";
import { withPrefix } from "gatsby";
import "../style/index.css"
import "../style/custom-style.sass"

const TemplateWrapper = ({ children }) => {
  const { title, description } = useSiteMetadata();
  return (
    <div>
      <Helmet>
        <html lang="en" />
        <title>{title}</title>
        <meta name="description" content={description} />

        <link
          rel="apple-touch-icon"
          sizes="180x180"
          href={`${withPrefix("/")}img/bytemath2.png`}
        />
        <link
          rel="icon"
          type="image/png"
          href={`${withPrefix("/")}img/bytemath2.png`}
          sizes="32x32"
        />
        {/* <link
          rel="mask-ico"
          href={`${withPrefix("/")}img/logo`}
          color="#ff4400"
        /> */}
        <link
          rel="icon"
          type="image/png"
          href={`${withPrefix("/")}img/bytemath2.png`}
          sizes="16x16"
        />
        
        {/* <meta name="theme-color" content="#fff" /> */}

        <meta property="og:type" content="business.business" />
        <meta property="og:title" content={title} />
        <meta property="og:url" content="/" />
        <meta
          property="og:image"
          content={`${withPrefix("/")}img/bytemath2.png`}
        />
      </Helmet>
      <Navbar />
      <div className="dark:bg-gray-800 dark:text-gray-100 bg-amber-50">{children}</div>
      <Footer />
    </div>
  );
};

export default TemplateWrapper;
