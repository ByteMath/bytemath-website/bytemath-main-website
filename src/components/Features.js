import React from "react";
import PropTypes from "prop-types";
import PreviewCompatibleImage from "../components/PreviewCompatibleImage";

const FeatureGrid = ({ gridItems }) => {
  return (
    <div className="grid gap-4 lg:grid-cols-2">
      {gridItems.map((item) => (
        <div key={item.text} className="p-4">
          <div className="text-center">
            <div className="w-60 mx-auto">
              <PreviewCompatibleImage imageInfo={item} />
            </div>
          </div>
          <p className="mt-4">{item.text}</p>
        </div>
      ))}
    </div>
  );
};

FeatureGrid.propTypes = {
  gridItems: PropTypes.arrayOf(
    PropTypes.shape({
      image: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
      text: PropTypes.string,
      content: PropTypes.object,
    })
  ),
};

export default FeatureGrid;
