//With this file we aim to let any part of our aplication to to be able to 
//determine the active theme (light or dark). Furthermore, will also store 
//the current theme in local storage so the theme is saved for the next 
//time the user visits the app.


import React, { createContext, useState, useEffect } from 'react';

//This function's job is to check the user's appearance preference, 
//and if they have set it to dark mode, the function returns dark 
//as the value. If the user has not selected dark as their default 
//system appearance, this function returns light.

const getInitialTheme = () => {
    if (typeof window !== 'undefined' && window.localStorage) {
        const storedPrefs = window.localStorage.getItem('theme');
        if (typeof storedPrefs === 'string') {
            return storedPrefs;
        }

        const userMedia = window.matchMedia('(prefers-color-scheme: dark)');
        if (userMedia.matches) {
            return 'dark';
        }
    }

    // If you want to use dark theme as the default, return 'dark' instead
    return 'light';
};

//declare and export a variable which holds some state context. 
export const ThemeContext = createContext();

export const ThemeProvider = ({ initialTheme, children }) => {
    const [theme, setTheme] = useState(getInitialTheme);

    const rawSetTheme = (rawTheme) => {
        //This function grabs the element that is the root element 
        //of the document in our React application.
        const root = window.document.documentElement;
        //Bolean checling if dark == true an set 'dark' in rawTheme 
        const isDark = rawTheme === 'dark';

        root.classList.remove(isDark ? 'light' : 'dark');
        root.classList.add(rawTheme);
        
        //store the user's theme value in local storage so that it 
        //is saved for the next time the user visits the web app
        localStorage.setItem('theme', rawTheme);
    };

    //checks to see if an initial theme value was passed to the component.
    if (initialTheme) {
        rawSetTheme(initialTheme);
    }

    useEffect(() => {
        const root = window.document.documentElement;
        root.classList.remove(theme === 'dark' ? 'light' : 'dark');
        root.classList.add(theme);
      }, [theme]);

    //renders the elements passed as children to this component and wraps 
    //them in ThemeContext.Provider so all of the children have access to 
    //theme and setTheme
    return (
        <ThemeContext.Provider value={{ theme, setTheme }}>
            {children}
        </ThemeContext.Provider>
    );
};