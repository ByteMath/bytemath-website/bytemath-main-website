import * as React from "react";
import Wave from "../img/wave.svg?raw";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faGitlab, faInstagram, faYoutube } from '@fortawesome/free-brands-svg-icons'
import  Logo  from "../img/ByteMath_me.svg?raw";

class Footer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      repository: 'https://gitlab.com/ByteMath',
      type: FooterType.Fadeout,
    };
  }
  render() {
    const { repository, type } = this.state;

    return (
      <footer>
        {type === FooterType.Wave ? (
          <div className="w-full bg-repeat-x text-bytemath">
            <img src={Wave} className="w-full" />
          </div>
        ) : type === FooterType.Fadeout ? (
          <div className="h-[4vh] bg-gradient-to-b from-amber-50 dark:from-gray-800 to-bytemath dark:to-bytemath"></div>
        ) : (
          ''
        )}
      <div className="mx-auto w-full h-max-full p-12 py-6 lg:py-8 bg-bytemath">
        <div className="md:flex md:justify-between">
          <div className="mb-6 md:mb-0 md:w-1/3 sm:flex-grow p-5 hidden">
            <a href="https://www.bytemath.lorenzozambelli.it/" className="flex px-4 max-h-24 w-auto h-[80%]">
              <img src={Logo} className="w-full h-full" />
            </a>
          </div>
          <div className="text-[1rem] grid gap-20 grid-cols-2 sm:gap-6 sm:grid-cols-4 w-full">
          <div className="">
        <h2 className="mb-6 text-sm font-bold uppercase text-white">Learn</h2>
        <ul className="static">
          <li className="mb-3 sm:mb-2 flex flex-auto">
            <a href="/blog" className="inline-block py-0.5 w-auto">
              <span className="whitespace-nowrap w-auto text-white font-light text-opacity-80 hover:text-opacity-100 hover:font-semibold xl:hover:px-3 transition-all ease-in-out">
                Blog
              </span>
            </a>
          </li>
          <li className="mb-3 sm:mb-2 flex flex-auto">
            <a href="/project" className="inline-block py-0.5 w-auto">
              <span className="whitespace-nowrap w-auto text-white font-light text-opacity-80 hover:text-opacity-100 hover:font-semibold xl:hover:px-3 transition-all ease-in-out">
                Projects
              </span>
            </a>
          </li>
        </ul>
      </div>

      <div className="">
        <h2 className="mb-6 text-sm font-bold uppercase text-white">Other</h2>
        <ul className="static">
          <li className="mb-3 sm:mb-2 flex flex-auto">
            <a href="/admin" className="inline-block py-0.5 w-auto">
              <span className="whitespace-nowrap w-auto text-white font-light text-opacity-80 hover:text-opacity-100 hover:font-semibold xl:hover:px-3 transition-all ease-in-out">
                Admin
              </span>
            </a>
          </li>
          <li className="mb-3 sm:mb-2 flex flex-auto">
            <a href="/contact" className="inline-block py-0.5 w-auto">
              <span className="whitespace-nowrap w-auto text-white font-light text-opacity-80 hover:text-opacity-100 hover:font-semibold xl:hover:px-3 transition-all ease-in-out">
                Contact
              </span>
            </a>
          </li>
        </ul>
      </div>

      <div className="">
        <h2 className="mb-6 text-sm font-bold uppercase text-white">Follow Us!</h2>
        <ul className="flex">
          <li className="mb-3 sm:mb-2 flex flex-auto">
            <a href="https://instagram.com/bytemath_" className="inline-block text-[1.2rem] px-[0.1rem] transition-all hover:-mt-2 hover:pb-2 text-white">
              <FontAwesomeIcon icon={faInstagram} />
            </a>
          </li>
          <li className="mb-3 sm:mb-2 flex flex-auto">
            <a href="https://www.youtube.com/channel/UCnyveEbLc5HYP2JNhjz6WtQ" className="inline-block text-[1.2rem] px-[0.1rem] transition-all hover:-mt-2 hover:pb-2 text-white">
              <FontAwesomeIcon icon={faYoutube} />
            </a>
          </li>
          <li className="mb-3 sm:mb-2 flex flex-auto">
            <a href="https://gitlab.com/ByteMath" className="inline-block text-[1.2rem] px-[0.1rem] transition-all hover:-mt-2 hover:pb-2 text-white">
              <FontAwesomeIcon icon={faGitlab} />
            </a>
          </li>
        </ul>
        <ul className="text-white font-medium text-sm sm:text-xs py-2">
          <li>
            Email us at: <a className="font-bold" href="mailto:bytemath@lorenzozambelli.it">bytemath@lorenzozambelli.it</a>
          </li>
        </ul>
      </div>

      <div className="">
        <h2 className="mb-6 text-sm font-bold uppercase text-white">Legal</h2>
        <ul className="static">
          <li className="mb-3 sm:mb-2 flex flex-auto">
            <a href="/privacy-policy" className="inline-block py-0.5 w-auto">
              <span className="whitespace-nowrap w-auto text-white font-light text-opacity-80 hover:text-opacity-100 hover:font-semibold xl:hover:px-3 transition-all ease-in-out">
                Privacy Policy
              </span>
            </a>
          </li>
          <li className="mb-3 sm:mb-2 flex flex-auto">
            <a href="/disclaimer" className="inline-block py-0.5 w-auto">
              <span className="whitespace-nowrap w-auto text-white font-light text-opacity-80 hover:text-opacity-100 hover:font-semibold xl:hover:px-3 transition-all ease-in-out">
                Disclaimer
              </span>
            </a>
          </li>
        </ul>
      </div>
    </div>
        </div>
        {/* <!--Start footer copyright--> */}
        <div className="items-center text-center py-5">
          <span className="text-xs text-white font-[200]">
            ByteMath - &copy;Lorenzo Zambelli
            <p>
              <a href={repository}>
                <span className="hover:font-[300] text-white">
                <FontAwesomeIcon icon={faGitlab} />
                  Source Code
                </span>
              </a>
            </p>
          </span>
        </div>
        {/* <!--End footer copyright--> */}
      </div>
    </footer>
 );
}
}

export const FooterType = {
None: 0,
Wave: 1,
Fadeout: 2,
};

export default Footer;