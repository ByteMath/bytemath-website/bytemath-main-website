// Setting background of all pages white in light mode -> black in dark mode
import * as React from "react";


const Background = ({children}) => (
    // Remove transition-all to disable the background color transition.
        <body className="flex flex-col bg-[#FCF0C4] dark:bg-gray-800 transition-all">
            {children}
        </body>
)

export default Background;

