import React, { useState } from "react";
import PropTypes from "prop-types";
import { Link } from "gatsby";
import ThemeToggle from "../components/themeToggle";

const Navbar = () => {
  const [isActive, setIsActive] = useState(false);

  return (
    <nav className="bg-bytemath flex flex-wrap font-black border-gray-200 p-4 md:flex w-auto items-center z-10" role="navigation" aria-label="main-navigation">
      <div className="container mx-auto">
        <div className="flex items-center">
          <Link to="/" className="whitespace-nowrap text-3xl font-black uppercase text-white" title="Logo">
            BYTEMATH
          </Link>
          {/* Hamburger menu */}
          <button
            className={`md:hidden ml-auto block text-white focus:outline-none`}
            aria-expanded={isActive}
            onClick={() => setIsActive(!isActive)}
          >
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" className={`h-6 w-6 fill-current text-gray-200`}>
              <path d="M0 96C0 78.3 14.3 64 32 64H416c17.7 0 32 14.3 32 32s-14.3 32-32 32H32C14.3 128 0 113.7 0 96zM0 256c0-17.7 14.3-32 32-32H416c17.7 0 32 14.3 32 32s-14.3 32-32 32H32c-17.7 0-32-14.3-32-32zM448 416c0 17.7-14.3 32-32 32H32c-17.7 0-32-14.3-32-32s14.3-32 32-32H416c17.7 0 32 14.3 32 32z"/>
              </svg>
          </button>
        </div>
        <div className={`md:flex items-center md:w-auto ${isActive ? "block" : "hidden"}`}>
          <ul className="md:flex md:flex-row md:space-x-4 justify-center">
            <li>
              <Link className="block px-6 py-2 text-black uppercase rounded hover:bg-gray-100 md:hover:bg-transparent md:hover:text-[#173494] md:px-0" to="/about">
                About
              </Link>
            </li>
            <li>
              <Link className="block px-6 py-2 text-black uppercase rounded hover:bg-gray-100 md:hover:bg-transparent md:hover:text-[#173494] md:px-0" to="/blog">
                Blog
              </Link>
            </li>
            <li>
              <Link className="block px-6 py-2 text-black uppercase rounded hover:bg-gray-100 md:hover:bg-transparent md:hover:text-[#173494] md:px-0" to="/project">
                Projects
              </Link>
            </li>
            <li>
              <Link className="block px-6 py-2 text-black uppercase rounded hover:bg-gray-100 md:hover:bg-transparent md:hover:text-[#173494] md:px-0" to="/contact">
                Contact
              </Link>
            </li>
            <li className={`${isActive ? "block px-6 py-2" : ""}`} >
              <ThemeToggle />
            </li>
          </ul>
        </div>
      </div>
      
    </nav>
  );
};

export default Navbar;

