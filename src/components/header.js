import React from "react";
import { Alert } from "flowbite-react";
import { Navbar } from 'flowbite-react';
import { DarkThemeToggle } from 'flowbite-react';


const Header = () => {

  return (
    <>
    <Navbar className="bg-bytemath font-black text-white border-gray-200 dark:border-gray-600 dark:bg-gray-900 p-4">
      <Navbar.Brand href="/">
        <span className="self-center whitespace-nowrap text-3xl font-black uppercase dark:text-white">
          BYTEMATH
        </span>
      </Navbar.Brand>
      
      <Navbar.Collapse >
        <Navbar.Link
          active
          href="/blog"
          class="block px-6 py-2 pl-3 pr-4  uppercase rounded hover:bg-gray-100 md:hover:bg-transparent md:hover:text-[#173494] md:p-0 md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-blue-500 md:dark:hover:bg-transparent dark:border-gray-700"
        >
          Blog
        </Navbar.Link>
        <Navbar.Link 
          href="/books"
          class="block px-6 py-2 pl-3 pr-4  uppercase  rounded hover:bg-gray-100 md:hover:bg-transparent md:hover:text-[#173494] md:p-0 md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-blue-500 md:dark:hover:bg-transparent dark:border-gray-700"
        >
          Books and resources
        </Navbar.Link>
        
        <Navbar.Link 
          href="/about"
          class="block px-6 py-2 pl-3 pr-4  uppercase rounded hover:bg-gray-100 md:hover:bg-transparent md:hover:text-[#173494] md:p-0 md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-blue-500 md:dark:hover:bg-transparent dark:border-gray-700"
        >
          About us
        </Navbar.Link>
        <Navbar.Link 
          href="/contact"
          class="block px-6 py-2 pl-3 pr-4  uppercase rounded hover:bg-gray-100 md:hover:bg-transparent md:hover:text-[#173494] md:p-0 md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-blue-500 md:dark:hover:bg-transparent dark:border-gray-700"
        >
          Contact
        </Navbar.Link>
      </Navbar.Collapse>
      <div className="flex place-content-end md:order-2">
        <DarkThemeToggle />
        <Navbar.Toggle />
      </div>
      </Navbar>
      
        
      <Alert color="failure">
        <span>
            <svg class="flex-shrink-0 inline w-4 h-4 mr-3" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 20 20"><path d="M10 .5a9.5 9.5 0 1 0 9.5 9.5A9.51 9.51 0 0 0 10 .5ZM9.5 4a1.5 1.5 0 1 1 0 3 1.5 1.5 0 0 1 0-3ZM12 15H8a1 1 0 0 1 0-2h1v-3H8a1 1 0 0 1 0-2h2a1 1 0 0 1 1 1v4h1a1 1 0 0 1 0 2Z"/></svg>
            <span class="font-medium">Attention!</span> This website is currently in development
        </span>
      </Alert>
      </>
  );
};
export default Header
