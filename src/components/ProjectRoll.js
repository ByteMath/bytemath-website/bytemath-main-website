import React from 'react'
import PropTypes from 'prop-types'
import { Link, graphql, StaticQuery } from 'gatsby'
import PreviewCompatibleImage from './PreviewCompatibleImage'
import { kebabCase } from "lodash";

const ProjectRollTemplate = (props) => {
  
  const { edges: posts } = props.data.allMarkdownRemark;

  return (
    <div className="max-w-sm px-4 py-1 mx-auto md:max-w-none">
      {posts &&
        posts.map(({ node: post }) => (
          <>
          
          <div className="lg:px-8 py-10 space-y-5 flex flex-col  rounded-lg lg:even:border-l-0 lg:even:border-r-8 border-l-8 even:border-primary border-secondary lg:even:flex-row-reverse flex-wrap" key={post.id}>
            <article
              className={`max-w-sm px-4 py-1 mx-auto md:max-w-none grid bg-gray-900 rounded-lg md:grid-cols-2 gap-6 md:gap-8 lg:gap-12 xl:gap-16 items-center  ${
                post.frontmatter.featuredpost ? 'is-featured' : ''
              }`}
            >
              <a class="relative block group" href="#0">
                <div class="absolute inset-0 bg-gray-800 hidden md:block transform md:translate-y-2 md:translate-x-4 xl:translate-y-4 xl:translate-x-8 group-hover:translate-x-0 group-hover:translate-y-0 transition duration-700 ease-out pointer-events-none" aria-hidden="true"></div>
                    {post?.frontmatter?.featuredimage && (
                    <figure class="relative h-0 pb-[56.25%] md:pb-[75%] lg:pb-[56.25%] overflow-hidden transform md:-translate-y-2 xl:-translate-y-4 group-hover:translate-x-0 group-hover:translate-y-0 transition duration-700 ease-out">
                    <PreviewCompatibleImage
                      imageInfo={{
                        image: post.frontmatter.featuredimage,
                        alt: `featured image thumbnail for post ${post.frontmatter.title}`,
                        width:
                          post.frontmatter.featuredimage.childImageSharp
                            .gatsbyImageData.width,
                        height:
                          post.frontmatter.featuredimage.childImageSharp
                            .gatsbyImageData.height,
                      }}
                    />
                    </figure>
                ) }
            </a>
              <header>
              <div className="flex mt-4">
                <div className="ml-auto flex">
                  {post.frontmatter.tags.map((tag) => (
                    <div key={tag} className="m-1 inline-flex text-center text-white py-1 space-x-4 rounded-full even:bg-blue-500 bg-purple-500 hover:bg-purple-600 hover:even:bg-blue-600 transition duration-150 ease-in-out">
                      <Link to={`/tags/${kebabCase(tag)}/`} className="inline-flex text-center text-gray-100 py-1 px-3 rounded-full  transition duration-150 ease-in-out">
                      {tag}
                      </Link>
                    </div>
                  ))}
                </div>
              </div>
                <h3 class="text-2xl lg:text-3xl font-bold leading-tight mb-2">
                  <Link
                    className="hover:text-gray-100 transition duration-150 ease-in-out text-white"
                    to={post.fields.slug}
                  >
                    {post.frontmatter.title}
                  </Link>
                </h3>
                  <span className="text-gray-500">
                    {post.frontmatter.date}
                  </span>
              
              <p className="text-lg text-gray-400 flex-grow">
                {post.excerpt}
                <br />
                <br />
                <Link className="md:col-start-2 row-start-1" to={post.fields.slug}> 
                <button type="button" class="text-[#01bfa6] hover:text-opacity-100 hover:font-semibold xl:hover:px-3 transition-all ease-in-out text-md font-regular px-3 py-2 rounded flex items-center mb-2">
                Keep Reading
                  <svg class="rtl:rotate-180 w-3.5 h-3.5 ms-2" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 10">
                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M1 5h12m0 0L9 1m4 4L9 9"/>
                  </svg>
                </button>
                </Link>
              </p>
              </header>
            </article>
          </div>
          </>
        ))}
    </div>
  )
}

ProjectRoll.propTypes = {
  data: PropTypes.shape({
    allMarkdownRemark: PropTypes.shape({
      edges: PropTypes.array,
    }),
  }),
}


export default function ProjectRoll() {
  return (
    <StaticQuery
      query={graphql`
        query ProjectRollQuery {
          allMarkdownRemark(
            sort: { order: DESC, fields: [frontmatter___date] }
            filter: { frontmatter: { templateKey: { eq: "project-post" } } }
          ) {
            edges {
              node {
                excerpt(pruneLength: 400)
                id
                fields {
                  slug
                }
                frontmatter {
                  title
                  templateKey
                  date(formatString: "MMMM DD, YYYY")
                  featuredpost
                  tags
                  featuredimage {
                    childImageSharp {
                      gatsbyImageData(
                        quality: 100
                        layout: CONSTRAINED
                      )

                    }
                  }
                }
              }
            }
          }
        }
      `}
      render={(data, count) => <ProjectRollTemplate data={data} count={count} />}
    />
  );
}
