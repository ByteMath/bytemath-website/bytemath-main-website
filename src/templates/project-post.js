import React from "react";
import PropTypes from "prop-types";
import { kebabCase } from "lodash";
import { Helmet } from "react-helmet";
import { graphql, Link } from "gatsby";
import Layout from "../components/Layout";
import Content, { HTMLContent } from "../components/Content";
import FullWidthImage from "../components/FullWidthImage";
import { getImage } from "gatsby-plugin-image";
import { GatsbyImage } from "gatsby-plugin-image";
import "../style/index.css";

export const BlogPostTemplate = ({
  content,
  contentComponent,
  description,
  tags,
  title,
  helmet,
  image,
  reference,
}) => {
  const PostContent = contentComponent || Content;
  const heroImage = getImage(image) || image;

  return (
    <section>
      {helmet || ""}
      <div className="mx-auto sm:p-10 md:p-16 dark:text-gray-100">
        <div className="flex flex-col max-w-4xl mx-auto overflow-hidden rounded">
          <div className="w-full md:w-4/5 mx-auto">
            <div className="space-y-2 py-4">
              <h1 className="text-4xl font-bold text-heading dark:text-white font-noe">{title}</h1>
            </div>
            <div className="text-base text-primary dark:text-gray-400 font-merriweather px-5 pb-5 pt-2">
              <em><PostContent content={description} /></em>
            </div>
          </div>
          {image ? (
            <figure className="flex justify-center">
              <GatsbyImage image={heroImage} className="w-full max-w-full h-auto dark:bg-gray-500" alt="" />
            </figure>
          ) : null}                 
        {reference ? (
          <div className="text-base text-center text-primary dark:text-gray-400 text-normal">
            <p className="border-b dark:border-gray-300">{reference}</p>
          </div>
        ) : null}
          <div className="p-6 pb-12 m-4 mx-auto mt-16 space-y-6 lg:max-w-3xl sm:px-10 sm:mx-12 lg:rounded-md dark:bg-gray-900">
            <div className="dark:text-gray-400 text-base text-primary font-merriweather py-2">
              <PostContent content={content} />
            </div>
            <div>
              {tags && tags.length ? (
                <div className="dark:text-white py-6 gap-2 border-t border-dashed border-black dark:border-gray-400" style={{ marginTop: `4rem` }}>
                  <h4 >Tags</h4>
                  <ul className="flex flex-wrap text-xs font-medium -m-1">
                    {tags.map((tag) => (
                      <li key={tag + `tag`} className="m-1">
                        <Link to={`/tags/${kebabCase(tag)}/`} className="inline-flex text-center text-gray-100 py-1 px-3 rounded-full bg-blue-500 hover:bg-blue-600 transition duration-150 ease-in-out">{tag}</Link>
                      </li>
                    ))}
                  </ul>
                </div>
              ) : null}
              
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

BlogPostTemplate.propTypes = {
  content: PropTypes.node.isRequired,
  contentComponent: PropTypes.func,
  description: PropTypes.node,
  title: PropTypes.string,
  reference: PropTypes.string,
  helmet: PropTypes.object,
  image: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
};

const BlogPost = ({ data }) => {
  const { markdownRemark: post } = data;

  return (
    <Layout>
      <BlogPostTemplate
        image={post.frontmatter.image}
        content={post.html}
        contentComponent={HTMLContent}
        description={post.frontmatter.description}
        helmet={
          <Helmet titleTemplate="%s | Blog">
            <title>{`${post.frontmatter.title}`}</title>
            <meta
              name="description"
              content={`${post.frontmatter.description}`}
            />
          </Helmet>
        }
        tags={post.frontmatter.tags}
        title={post.frontmatter.title}
        reference={post.frontmatter.reference}
      />
    </Layout>
  );
};

BlogPost.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.object,
  }),
};

export default BlogPost;

export const pageQuery = graphql`
  query BlogPostByID($id: String!) {
    markdownRemark(id: { eq: $id }) {
      id
      html
      frontmatter {
        date(formatString: "MMMM DD, YYYY")
        title
        description
        tags
        image {
          childImageSharp {
            gatsbyImageData(quality: 100, layout: FULL_WIDTH)
          }
        }
        reference
      }
    }
  }
`;

