import React from "react";
import PropTypes from "prop-types";
import { Link, graphql } from "gatsby";
import { getImage } from "gatsby-plugin-image";

import Layout from "../components/Layout";
import Features from "../components/Features";
import BlogRoll from "../components/BlogRoll";
import ProjectRoll from "../components/ProjectRoll";
import FullWidthImage from "../components/FullWidthImage";

export const IndexPageTemplate = ({
  image,
  title,
  heading,
  subheading,
  mainpitch,
  description,
  intro,
}) => {
  const heroImage = getImage(image) || image;

  return (
    <div>
      <FullWidthImage img={heroImage} title={title} subheading={subheading} />
      <section className="section section--gradient">
        <div className="container mx-auto">
          <div className="section">
            <div className="columns">
              <div className="column lg:is-10 lg:mx-auto">
                <div className="content">
                  <div className="mb-8">
                    <div className="p-4">
                      <h1 className="text-3xl font-bold text-gray-800 dark:text-white">{mainpitch.title}</h1>
                    </div>
                    <div className="p-4 mt-4">
                      <h3 className="text-xl text-gray-600 dark:text-gray-400">{mainpitch.description}</h3>
                    </div>
                  </div>
                  <div className="grid flex-col grid-cols-1 mb-8">
                    <div className="gap-4">
                      <h3 className="font-semibold text-3xl dark:text-white p-4">
                        {heading}
                      </h3>
                      <p className="dark:text-gray-400 p-4">{description}</p>
                    </div>
                  </div>
                  <Features gridItems={intro.blurbs} />
                  <div className="columns mb-8">
                    <div className="text-center">
                      <Link className="btn" to="/project">
                        See all projects
                      </Link>
                    </div>
                  </div>
                  <div className="flex flex-col justify-center antialiased text-gray-200">
                    <h3 className="font-semibold text-3xl dark:text-white p-4">
                      Latest stories
                    </h3>
                    <div className="flex sm:max-w-full mb-16 lg:pl-0 pl-4 mx-auto lg:even:flex-row-reverse flex-wrap">
                      <BlogRoll />
                    </div>
                    <div className="text-center p-4">
                      <Link className="btn" to="/blog">
                        Read more
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

IndexPageTemplate.propTypes = {
  image: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  title: PropTypes.string,
  heading: PropTypes.string,
  subheading: PropTypes.string,
  mainpitch: PropTypes.object,
  description: PropTypes.string,
  intro: PropTypes.shape({
    blurbs: PropTypes.array,
  }),
};

const IndexPage = ({ data }) => {
  const { frontmatter } = data.markdownRemark;

  return (
    <Layout>
      <IndexPageTemplate
        image={frontmatter.image}
        title={frontmatter.title}
        heading={frontmatter.heading}
        subheading={frontmatter.subheading}
        mainpitch={frontmatter.mainpitch}
        description={frontmatter.description}
        intro={frontmatter.intro}
      />
    </Layout>
  );
};

IndexPage.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.shape({
      frontmatter: PropTypes.object,
    }),
  }),
};

export default IndexPage;

export const pageQuery = graphql`
  query IndexPageTemplate {
    markdownRemark(frontmatter: { templateKey: { eq: "index-page" } }) {
      frontmatter {
        title
        image {
          childImageSharp {
            gatsbyImageData(quality: 100, layout: FULL_WIDTH)
          }
        }
        heading
        subheading
        mainpitch {
          title
          description
        }
        description
        intro {
          blurbs {
            image {
              childImageSharp {
                gatsbyImageData(width: 240, quality: 64, layout: CONSTRAINED)
              }
            }
            text
          }
          heading
          description
        }
      }
    }
  }
`;
