# ByteMath app

This is the main website of ByteMath. This website has as base the starter Gatbsy-Netlify blog.

## Prerequisites

- Minimal Node.js version 14.15.0
- [Gatsby CLI](https://www.gatsbyjs.com/docs/reference/gatsby-cli/)
- [Netlify CLI](https://github.com/netlify/cli)

## Access Locally

Pulldown a local copy of the Gitlab repository,

```
$ git clone https://gitlab.com/[GITLAB_USERNAME]/[REPO_NAME].git
$ cd [REPO_NAME]
$ yarn
$ netlify dev # or ntl dev
```

This uses [Netlify Dev](https://www.netlify.com/products/dev/?utm_source=blog&utm_medium=netlifycms&utm_campaign=devex) CLI feature to serve any functions you have in the `netlify/functions` folder.

To test the CMS locally, you'll need to run a production build of the site:

```
$ npm run build
$ netlify dev # or ntl dev
```

or

```
$ yarn start
```

### Access locally using docker

To run locally this app you can also use docker by simply running 

```
$ docker-compose up --build
```

the first time, and then just

```
$ docker-compose up
```

### Setting up the CMS

Follow the [Decap CMS Quick Start Guide](https://www.netlifycms.org/docs/quick-start/#authentication) to set up authentication, and hosting for production.

If you want use Decap CMS locally, run the site in one terminal with `npm run start` or `yarn start` and in another
Terminal you can use `npx netlify-cms-proxy-server` which proxy requests so you'll be automatically logged
in as a user on [http:localhost:8000/admin](http:localhost:8000/admin).

## Purgecss

This plugin uses [gatsby-plugin-purgecss](https://www.gatsbyjs.org/packages/gatsby-plugin-purgecss/) and [bulma](https://bulma.io/). The bulma builds are usually ~170K but reduced 90% by purgecss.

