# Use Node.js image as base
FROM node:latest AS build

# Set the working directory in the container
WORKDIR /app

# Copy package.json and yarn.lock to the container
COPY package.json yarn.lock ./

# Install dependencies
RUN yarn install

# Install Gatsby CLI globally
RUN yarn global add gatsby-cli

# Copy the rest of the application code
COPY . .

# Build the application
RUN gatsby build

# Use Nginx image as deploy stage
FROM nginx:1.18-alpine AS deploy

# Set the working directory in the container
WORKDIR /usr/share/nginx/html

# Remove default Nginx static files
RUN rm -rf ./*

# Copy the built application from the build stage to the deploy stage
COPY --from=build /app/public .

# Expose port 80
EXPOSE 80

# Command to run NGINX
CMD ["nginx", "-g", "daemon off;"]

